# ITWS-4960/6960 Database Systems

Database systems is an advanced undergraduate-level/graduate-level class intended to give students in the Information Technology and Web Sciences program a strong foundation of knowledge related to modern database management systems.

## Course Information

Database Systems ITWS-4960/6960

Wednesday evenings from 6:00pm - 8:50pm

The course will meet in Low - 3039

Course Website: [https://gitlab.com/samuelbjohnson/fall-2019-itws-4960-6960](https://gitlab.com/samuelbjohnson/fall-2019-itws-4960-6960)

Slack team: [https://itws-databases-f19.slack.com](https://itws-databases-f19.slack.com)

## Prerequisites

This course will involve a good amount of programming. It requires a good working knowledge of data structures and algorithms, and proficiency in a higher-level programming language (e.g., C++ or Python (equivalent of CSCI-2300)). Homework assignments that require programming not covered in class will be in Python. No prior knowledge of database management systems is assumed. 

## Instructors

### Professor

Samuel B. Johnson

johnss20 \[at\] rpi \[dot\] edu

Office hours:
- Wednesday evenings after class
- By appointment

## Course Goals and Objectives

By the conclusion of this course, students should have learned the following:

- Be able to apply principles of normalization to design a data model that leads to the development of high performance data-intensive applications. Students taking the course for graduate credit will be expected to demonstrate this knowledge at a higher level of complexity.
- Be able to write correct and efficient code that implements application logic for high throughput data operations.
- Understand how a database management system integrates into a larger application architecture.
- Have a solid understanding of a variety of database paradigms, and a good idea of when each is appropriate. Students taking the course for graduate credit will be expected to build on their understanding of database paradigms to use multiple paradigms appropriately in an application.

## Assessment Measures and Grading Criteria

This is a programming-intensive course, so a majority of your grade will be determined by your work on a number of individual programming assignments, and a final team project. There will also be a mid-term exam, covering approximately the first half of the semester, and a number of surprise in-class quizzes. Your grade will be determined as follows:

- Homework (40%). There will be six homework assignments given throughout the semester. Two will be written, the others will involve some element of programming. The percentage of your grade will vary, but homework will make up forty percent of your final grade.
- In-class Quizzes (20%). There will be between four and eight in-class quizzes, covering topics discussed in the reading for that class and reviewed in the lecture for that day. Quizzes will not be announced ahead of time. Each quiz will make up an equal portion of the 20%, and the lowest quiz grade will be dropped.
- Midterm Exam (10%). There will be a single exam given during the semester: a midterm, covering approximately the first half of the course, worth 10 percent of your grade.
- Final Project (30%). There will be one final project, to be done either individually or in teams of two and presented on the last day of class. For students taking the course for graduate credit, there will be an additional requirement to this project, beyond what is expected of undergraduates.

## Texts

The course will rely extensively on **Database Systems The Complete Book (2nd Edition)** by Hector Garcia-Molina, Jeffrey D. Ullman, and Jennifer Widom. ISBN: 0131873253, Prentice Hall.

Additional readings will be assigned from a number of other online sources. All will be either freely available, or available for free to the RPI community.

## Communication

There is a [Slack team](https://itws-databases-f19.slack.com) for the course. Students are encouraged to use this as the primary means of communication between each other and with the instructor, including any questions related to the course material or assignments.

If there's something you don't feel comfortable sharing in slack, the instructor is available by email, or you can always stop by office hours or set up an appointment.

Announcements (e.g., changes to scheduled reading or homework assignments) will be posted on the [README](README.md) page of the course gitlab repository.

## Other Course Policies

You are expected to communicate to the instructor any issue regarding your performance in the class ahead of time. This includes absence from exams, late homeworks, inability to perform an assigned task, problems with your team members, the need for extra time on exams, etc. You should be prepared to provide sufficient proof of any circumstances on which you are making a special request as outlined in the Rensselaer Handbook of Student Rights and Responsibilities.

### Attendence

Attendence is not required, though it is highly encouraged. Students are accountable for all material taught throughout the semester, and being absent when the material was covered is not an acceptable excuse for not knowing it. If a quiz is given during class, absence from class will mean a zero on that quiz, unless the absence was excused ahead of time.

### Late Policy

Late assignments create an extra burden on the instructor and delay the discussion of the solutions in class. Homework assignments must be submitted electronically by the deadline, as measured by our computers. Assignments that are a minute late are considered a day late! Each student will be given a total of three days (whole or partial) of grace for late assignments for the whole semester. These late days should be used carefully. Once the late days have been exhausted, late assignments may not be accepted without a written excuse from the Student Experience office.

### Grade Appeal Policy

If you disagree with the grading on an assignment, you should appeal to the instructor. Appeals must be made within one week after the specific grade is returned.

### Taking Exams

The midterm exam will be open book and open notes. You may not use any electronic tools during the exam including cell phones, tablets and calculators, and you may not share your notes with anybody during the exam.

If you will be absent from the exam or have other circumstances requiring exceptions, you must get an official excuse from the Student Experience office (se@rpi.edu).

## Academic Integrity

Student-teacher relationships are built on trust. For example, students must trust that teachers have made appropriate decisions about the structure and content of the courses they teach, and teachers must trust that the assignments that students turn in are their own. Acts that violate this trust undermine the educational process. The Rensselaer Handbook of Student Rights and Responsibilities defines various forms of Academic Dishonesty and you should make yourself familiar with these. In this class, all assignments that are turned in for a grade must represent the student’s own work. In cases where help was received, or teamwork was allowed, a notation on the assignment should indicate your collaboration.

Cheating and Academic Dishonesty will not be tolerated. All your coursework should provide an honest effort in solving the assigned problem by yourself (and by your group partners for group assignments). You are allowed to work with other students in designing algorithms, in interpreting error messages, in discussing strategies for finding bugs, but NOT in writing code or writing down solutions. Even if you discuss the problems with other students (or other teams), you should write down your own solution or program when turning in an assignment.

You may not share, copy, or discuss in detail code or solutions while writing it or afterwards. You may not show your code or solutions to other students as a means of helping them. You may not leave online, printed copies or drafts of your solutions in publicly accessible areas, such as labs, workstations, dorms, etc. You may not post complete or partial answers to homeworks in any public forum, especially on Slack before the due date to make sure all students had a chance to work on the problems on their own.

All cases of cheating will be punished and reported to the Dean of Students. You will receive a grade of zero if you cheat on a homework. A second instance of cheating on a homework or any cheating in the exam will result in an F grade in this course. If you have any question concerning this policy before submitting an assignment, please ask for clarification. There may be changes to the policies, deadlines and list of topics described in the syllabus. You can expect me to give you reasonable notice of any changes. All changes will be announced in class.
