This repository will contain the [syllabus](syllabus.md), class notes (including code snippets), homework assignments for the Fall 2019 ITWS 4960/6960 Database Systems course at RPI.

## Announcements

**Sunday September 8** Our [Submitty](https://submitty.cs.rpi.edu/f19/itws4960) site is up and all registered students should be able to view it.

### Welcome

Welcome to ITWS 4960/6960 Database Systems. Take some time to read through the [syllabus](syllabus.md).

## Git

As you've noticed, rather than a traditional website, this course uses a git repository. The goal is both to make it easy for you to retrieve correct copies of sample code and homework (much of which will involve runnable code) and to help you become familiar with a tool that's widely used in both academia and industry. 

If you're not familiar with git, take an hour and familiarize yourself with it. There are some good resources [here](https://try.github.io/). *This isn't a requirement&mdash;it will be possible to successfully complete this course without a working knowledge of git. But you'll need to learn it eventually anyway, so why not now.*

### Git-related course policies

One of the side effects of using git is that it becomes easy for students to help correct errors (or make suggestions) in any of the content hosted here. If you find an error (typo, bug, other mistake, etc.), feel free to fork the repository, and make a pull request (I believe GitLab calls it a "merge request").

Some of the homework assignments or sample code will become visible in branches throughout the semester before it's technically assigned. If it's in the repo, feel free to look at it. But until I merge it into the `master` branch, everything is still subject to change without notice. If you start a homework assignment early, be sure to check back for changes (which git makes easy to do).


